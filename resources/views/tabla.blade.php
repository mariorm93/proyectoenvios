<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Calixta</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet">

    <style type="text/css">
        :root {
            --color-background: #f6f6f6;
            --color-principal-claro: #c71941;
            --color-principal: #b5173b;
            --color-principal-oscuro: #a71537;
        }
    </style>

    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/paletas-color.css">
    <link rel="stylesheet" href="css/tabs.css">
</head>

<body class="top-navigation">
    <div id="wrapper">
        <div id="page-wrapper" invitado>
            <div class="wrapper wrapper-content">
                <div class="row">

                    <!-- CONTENIDO -->
                    <div class="col-lg-12 p-w-lg">
                        <h1>Calixta</h1>

                        <form-table 
                        api_form="{{url('/formulario')}}" 
                        api_table="{{url('/pruebas')}}"
                        ibox space spinner>
                        </form-table>

                        
                    </div>
                    <!-- END CONTENIDO -->
                    
                </div>
            </div>
        </div>
    </div>

    <script src="js/general.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
