<?php

use Illuminate\Http\Request;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB
use Illuminate\Support\Collection;
use App\Users;
require(app_path().'/estados.php');


trait ObtencionTablas {

    public function queryTablMensEnvios($idEnvio)
    {

		//declaro la clase donde estará el resultado de ejecutar el query (esto es una colección)

		$tablaEnvios=DB::table('envios')
					->select('fechaInicio', 'id', 'fechaUltimoMovimiento')
					->where('id', '=', $idEnvio)
					->get();


		//realizamos las funciones para saber el espacio de tiempo a evaluar del Envio1
		$fechaInicio=$tablaEnvios->first()->fechaInicio;
		$fechaFinal=$tablaEnvios->first()->fechaUltimoMovimiento;

		//convertimos las fechas

		$fechaInicio = $this->doubleToTime($fechaInicio);
		$fechaFinal = $this->doubleToTime($fechaFinal);

		$fechaInicio=new \DateTime("@$fechaInicio");
    	$fechaFinal=new \DateTime("@$fechaFinal");
		

		//declarar la colección donde voy a almacenar la unión de los resultados
		$coleccionQueries=collect();


    		
		//Realizo las consultas 
		while($fechaInicio<=$fechaFinal){

	    	$fechaIndex = $fechaInicio->format("ym");

    		echo "mensajesenvios_$fechaIndex";
    		//declarar el resultado de ejecutar el query (esto es una colección)

	    	$arregloQueries=DB::table("mensajesenvios_$fechaIndex")
	                    ->select('idEnvio','estado','destino')
	                    ->where("idEnvio","=", "$idEnvio")
	                    ->limit(10)
	                    ->get();

            //agregr los elementos del resultado a una colección general
			

	        $coleccionQueries=$coleccionQueries->concat($arregloQueries);

	        date_add($fechaInicio, date_interval_create_from_date_string('1 months'));
	    }

	    return($coleccionQueries);	
    }

  
    //convierte las fechas que estan en double a formato Unix
	function doubleToTime($number)
	{
	    $dias=(integer)$number;
	    $dias-=25569;
	    $secs=$dias*24*3600;
	    $tiempo=$number-(integer)$number;
	    $tiempo=$tiempo*24*3600;
	    $secs+=$tiempo;
	    return (integer)$secs;
	}



}




?>
