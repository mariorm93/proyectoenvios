<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 23/08/2016
 * Time: 04:35 PM
 *
 * Clase Resultados
 */

namespace App;


use Response;
use Log;
use ForceUTF8\Encoding;

class Resultado{

    private $parametros = [];
    private $headers = [];
    private $menu = [];
    private $order_by = [];
    private $datos = [];

    public function __construct(array $parametros = []){
        $this->parametros = $parametros;
        $this->parametros['headers'] = true;
        $this->parametros['vacio'] = 'No se encontraron registros';
    }

    /**
     * Oculta los headers de la tabla
     */
    public function setHiddenHeaders(){
        $this->parametros['headers'] = false;
    }

    /**
     * Establece un parámetro general
     *
     * @param  string $nombreParam
     * @param  string $valorParam
     * @return void
     */
    public function setTableParam($nombreParam, $valorParam){
        $this->parametros[$nombreParam] = $valorParam;
    }

    /**
     * Agrega botones como opciones para cuando la tabla es seleccionable
     *
     * @param array $arrayBotones
     * @return void
     */
    public function addButtonsSelectableDataParam( array $arrayBotones ){
        $this->parametros['seleccionable']['activo'] = true;
        $this->parametros['seleccionable']['botones'] = $arrayBotones;
    }

    /**
     * Define que la tabla se pueda recargar automáticamente
     *
     * @param $segundos
     */
    public function setRefreshTableDataParam( $segundos ){
        $this->parametros['recargable']['activo'] = true;
        $this->parametros['recargable']['segundos'] = $segundos;
        $this->parametros['recargable']['ultimo'] = date('d/m/Y h:i:s');
    }

    /**
     * Agrega botones a la tabla
     *
     * @param array $arrayBotones
     */
    public function setButtonsDataParam( array $arrayBotones ){
        $this->parametros['botones'] = $arrayBotones;
    }

    /**
     * Devuelve el valor de un parámetro general
     *
     * @param  string $nombreParam
     * @return mixed
     */
    public function getTableParam($nombreParam){
        return $this->parametros[$nombreParam];
    }

    /**
     * Añade un header
     *
     * @param  string $nombre
     * @param  array $parametros
     * @return void
     */
    public function addHeader($nombre, array $parametros = []){
        $this->headers[$nombre] = $parametros;
    }

    /**
     * Establece un parámetro a un header en específico
     *
     * @param  string $nombreHeader
     * @param  string $nombreParam
     * @param  string $valorParam
     * @return void
     */
    public function setHeaderParam($nombreHeader, $nombreParam, $valorParam){
        $this->headers[$nombreHeader][$nombreParam] = $valorParam;
    }

    /**
     * Devuelve el valor de un parámetro de un header en específico
     *
     * @param  string $nombreHeader
     * @param  string $nombreParam
     * @return mixed
     */
    public function getHeaderParam($nombreHeader, $nombreParam){
        return $this->headers[$nombreHeader][$nombreParam]?:null;
    }

    /**
     * Añade un menu
     *
     * @param  string $nombre
     * @param  array $parametros
     * @return void
     */
    public function addMenu($nombre, array $parametros = []){
        $this->menu[$nombre] = $parametros;
    }

    /**
     * Establece un parámetro a un menú en específico
     *
     * @param  string $nombreMenu
     * @param  string $nombreParam
     * @param  string $valorParam
     * @return void
     */
    public function setMenuParam($nombreMenu, $nombreParam, $valorParam){
        $this->menu[$nombreMenu][$nombreParam] = $valorParam;
    }

    /**
     * Devuelve el valor de un parámetro de un menú en específico
     *
     * @param  string $nombreMenu
     * @param  string $nombreParam
     * @return mixed
     */
    public function getMenuParam($nombreMenu, $nombreParam){
        return $this->menu[$nombreMenu][$nombreParam]?:null;
    }

    /**
     * Establece los parámetros de ORDER BY
     *
     * @param  string $valor
     * @param  string $accion
     * @param  string $filtro
     * @return void
     */
    public function setOrderBy($valor, $accion, $filtro){
        $this->order_by = [
            'valor' => $valor,
            'accion' => $accion,
            'filtro' => $filtro
        ];
    }

    /**
     * Establece los parámetros de ORDER BY
     *
     * @return array order_by
     */
    public function getOrderBy(){
        return $this->order_by;
    }

    /**
     * Añade un dato
     *
     * @param  string $id
     * @param  array $parametros
     * @return void
     */
    public function addData($id, array $parametros = []){
        $this->datos[$id] = $parametros;
        $this->datos[$id]['items'] = [];

    }

    /**
     * Establece un parámetro a un dato en específico
     *
     * @param  string $idDato
     * @param  string $nombreParam
     * @param  string $valorParam
     * @return void
     */
    public function setDataParam($idDato, $nombreParam, $valorParam){
        $this->datos[$idDato][$nombreParam] = $valorParam;
    }

    /**
     * Devuelve el valor de un parámetro de un menú en específico
     *
     * @param  string $idDato
     * @param  string $nombreParam
     * @return mixed
     */
    public function getDataParam($idDato, $nombreParam){
        return $this->datos[$idDato][$nombreParam]?:null;
    }


    /**
     * Establece un parametro seleccionable para un conjunto de datos
     *
     * @param string $idDato
     * @param boolean $activo
     * @param string $valor
     *
     * @return void
     */
    public function setSelectableDataParam($idDato, $activo, $valor){
        $this->setDataParam( $idDato, 'seleccionable', [
            'activo' => $activo,
            'valor' => $valor
        ] );

        $this->parametros['seleccionable']['activo'] = true;
    }


    /**
     * Establece un item a un dato en específico
     *
     * @param  string $idDato
     * @param  string $tipo
     * @param  string $valor
     * @param  string $clase
     * @param  string $atributo
     * @param  string $valor_atributo
     *
     * @return void
     */
    public function addDataItem($idDato, $tipo, $valor = null, $clase = null, $atributo = null, $valor_atributo = null){
        if( !is_array($valor) )
            $valor = Encoding::toUTF8($valor);
        else
            foreach ($valor as $i => $item)
                $valor[$i] = Encoding::toUTF8($item);

        $data = [
            'tipo' => $tipo,
            'clase' => $clase,
            'atributo' => $atributo,
            'valor_atributo' => $valor_atributo,
            'valor' => $valor
        ];

        if($idDato == "__NO_RECARGABLE__"){
            $this->parametros["no_recargable"][] = $data;
        }else{
            $this->datos[$idDato]['items'][] = $data;
        }
    }

    public function createCol($clase_col, $api, $tipo, array $config = []){
        $clase = array_key_exists('clase', $config) ? $config['clase'] : 'widget white-bg m-sm';
        $spinner = array_key_exists('spinner', $config) ? $config['spinner'] : false;
        $ibox = array_key_exists('ibox', $config) ? $config['ibox'] : false;
        $clase_tabla = array_key_exists('clase_tabla', $config) ? $config['clase_tabla'] : 'tabla-generada';

        // Form Tabla
        $col_tabla = array_key_exists('col_tabla', $config) ? $config['col_tabla'] : 'col-md-12';
        $col_form = array_key_exists('col_form', $config) ? $config['col_form'] : 'col-md-12';
        $col_completa = array_key_exists('columna_completa', $config) ? $config['columna_completa'] : false;

        return [
            'clase_col' => $clase_col,
            'valor' => $api,
            'tipo' => $tipo,
            'clase' => $clase,
            'spinner' => $spinner,
            'ibox' => $ibox,
            'clase_tabla' => $clase_tabla,
            'col_tabla' => $col_tabla,
            'col_form' => $col_form,
            'columna_completa' => $col_completa
        ];
    }

    public function createRow(array $columnas){
        return $columnas;
    }

    public function addDataLayoutItem($idDato, array $renglones, $clase = null){
        $this->addDataItem($idDato, 'layout', $renglones, $clase);
    }

    /**
     * Establece un item NULO a un dato en específico
     *
     * @param  string $idDato
     *
     * @return void
     */
    public function addDataNullItem($idDato){
        $this->addDataItem($idDato, 'texto', null);
    }

    /**
     * Establece un item de tipo HTML a un dato en específico
     *
     * @param  string $idDato
     * @param  string $mensaje
     *
     * @return void
     */
    public function addDataHtmlItem($idDato, $mensaje, $clase = null, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'html', $mensaje, $clase, $atributo, $valor_atributo);
    }

    /**
     * Establece un item de tipo Multimedia a un dato en específico
     *
     * @param string $idDato
     * @param string $tipo
     * @param string $url
     *
     * @return void
     */
    public function addDataMultimediaItem($idDato, $tipo, $url){
        $this->addDataItem($idDato, 'multimedia', [
            'tipo' => $tipo,
            'url' => $url
        ]);
    }

    /**
     * Establece un item de tipo iFrame a un dato en específico
     *
     * @param  string $idDato
     * @param  string $mensaje
     *
     * @return void
     */
    public function addDataiFrameItem($idDato, $mensaje){
        $this->addDataItem($idDato, 'iframe', $mensaje);
    }

    /**
     * Establece un item de tipo TEXTO a un dato en específico
     *
     * @param  string $idDato
     * @param  string $mensaje
     * @param  string $clase
     * @param  string $atributo
     * @param  string $valor_atributo
     *
     * @return void
     */
    public function addDataTextItem($idDato, $mensaje, $clase = null, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'texto', $mensaje, $clase, $atributo, $valor_atributo);
    }

    /**
     * Establece un item de tipo input (FORM)
     *
     * @param $idDato
     * @param $input
     * @param null $clase
     * @param null $atributo
     * @param null $valor_atributo
     */
    public function addDataInputItem($idDato, $input, $clase = null, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'input', $input, $clase, $atributo, $valor_atributo);
    }

    /**
     * Establece un item para mostrar el preview de los servicios
     *
     * @param $idDato
     * @param $contenido
     * @param $tipo (SMS, MMS, MAIL)
     * @param $clase
     * @param $atributo
     * @param $valor_atributo
     *
     * @retun void
     */
    public function addDataPreviewItem($idDato, $contenido, $tipo, $clase = null, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'preview-servicio', [
            'contenido' => $contenido,
            'tipo' => $tipo
        ], $clase, $atributo, $valor_atributo);

    }

    /**
     * Prepara una tabla por API
     *
     * @param $titulo
     * @param $api
     * @return array
     */
    public function createApiPanel($titulo, $api){
        return ['titulo' => $titulo, 'api' => $api];
    }

    /**
     * Prepara un jsonTabla
     *
     * @param $titulo
     * @param $json
     * @return array
     */
    public function createJsonPanel($titulo, $json){
        return ['titulo' => $titulo, 'json' => $json];
    }

    /**
     * Dibuja Paneles con Tablas dentro
     *
     * @param $idDato
     * @param array $paneles - Arreglo con las tablas a dibujar [api => url, json => jsonTabla]
     */
    public function addDataPanelsItem($idDato, array $paneles){
        $this->addDataItem($idDato, 'paneles', $paneles);
    }

    /**
     * Agrega un item editable
     *
     * @param string $idDato
     * @param string $valor
     * @param string $accion
     * @param string $nombre
     * @param array $extras
     * @param string $label
     */
    public function addDataEditItem($idDato, $valor, $accion, $nombre, array $extras = [], $label = null){
        $this->addDataItem($idDato, 'editable', [
            'accion' => $accion,
            'valor' => $valor,
            'nombre' => $nombre,
            'extras' => $extras,
            'label' => $label
        ]);
    }

    /**
     * Establece un item de tipo ICONO a un dato en específico
     *
     * @param  string $idDato
     * @param string $clase
     *
     * @return void
     */
    public function addDataIconItem($idDato, $clase = null){
        $this->addDataItem($idDato, 'icono', ['clase' => $clase]);
    }

    /**
     * Establece un item de tipo LABEL a un dato en específico
     *
     * @param  string $idDato
     * @param  string $mensaje
     * @param string $clase
     * @param string $claseCelda
     * @param boolean $multiple
     *
     * @return void
     */
    public function addDataLabelItem($idDato, $mensaje, $clase, $claseCelda = null, $multiple = false){
        if ($mensaje) {
            $this->addDataItem($idDato, 'label', [
                'mensaje' => $mensaje,
                'clase' => $clase,
                'multiple' => $multiple
            ], $claseCelda);
        }
        else {
            $this->addDataNullItem($idDato);
        }
    }

    /**
     * Establece un item de tipo FuncionJS a un dato en específico
     *
     * @param  string $idDato
     * @param  string $funcion
     * @param string $parametro
     * @param string $clase
     * @param string $atributo
     * @param string $valor_atributo
     *
     * @return void
     */
    public function addDataFuncionJSItem($idDato, $funcion, $parametro = null, $clase = null, $atributo = null, $valor_atributo = null){
        if ($parametro) {
            $this->addDataItem($idDato, 'funcionjs', [
                'funcion' => $funcion,
                'parametro' => $parametro
            ], $clase, $atributo, $valor_atributo);
        }
        else {
            $this->addDataNullItem($idDato);
        }
    }

    /**
     * @param $titulo
     * @param $texto
     * @param $tipo
     * @param $confirmar
     * @param $cancelar
     * @param $html
     * @return array
     */
    public function createConfirmacion($tipo, $titulo, $texto, $confirmar = "Confirmar", $cancelar = "Cancelar", $html = false){
        return [
            'titulo' => $titulo,
            'texto' => $texto,
            'tipo' => $tipo,
            'texto_confirmar' => $confirmar,
            'texto_cancelar' => $cancelar,
            'html' => $html
        ];
    }

    /**
     * Crea un elemento de petición
     *
     * @param $tipo_peticion (get, post, delete)
     * @param $peticion - URL a ejecutar
     * @param array|null $parametros
     * @return array
     */
    public function createPeticion($tipo_peticion, $peticion, array $parametros = null){
        return [
            'tipo_peticion' => $tipo_peticion,
            'peticion' => $peticion,
            'parametros' => $parametros
        ];
    }

    /**
     * Crea un botón de tipo texto
     *
     * @param  string $texto
     * @param  string $clase
     * @param  string $tipo_destino:    modal, link, recargar, seleccionable, ajax
     * @param  mixed $accion
     * @param  string $titulo
     * @param  string $mostrar
     * @param  array $confirmar
     * @param  array $mensaje
     * @param  bool $disabled
     * @param  string $clipboard
     *
     * @return array
     */
    public function createButtonTextElement($texto, $clase, $tipo_destino, $accion, $titulo, $mostrar = 'supertabla',
                                            array $confirmar = null, array $mensaje = null, $disabled = false, $clipboard = null){
        return [
            'tipo' => 'texto',
            'valor' => $texto,
            'clase' => $clase,
            'tipo_destino' => $tipo_destino,
            'accion' => $accion,
            'titulo' => $titulo,
            'mostrar' => $mostrar,
            'confirmar' => $confirmar,
            'mensaje' => $mensaje,
            'disabled' => $disabled,
            'clipboard' => $clipboard
        ];
    }

    /**
     * Crea un botón de tipo ícono
     *
     * @param  string $icono
     * @param  string $clase
     * @param  string $tipo_destino:    modal, link, recargar, seleccionable, ajax
     * @param  mixed $accion
     * @param  string $titulo
     * @param  string $mostrar
     * @param  array $confirmar
     * @param  array $mensaje
     * @param  bool $disabled
     * @param  string $clipboard
     *
     * @return array
     */
    public function createButtonIconElement($icono, $clase, $tipo_destino, $accion, $titulo, $mostrar = 'supertabla',
                                            array $confirmar = null, array $mensaje = null, $disabled = false, $clipboard = null){
        return [
            'tipo' => 'icono',
            'valor' => $icono,
            'clase' => $clase,
            'tipo_destino' => $tipo_destino,
            'accion' => $accion,
            'titulo' => $titulo,
            'mostrar' => $mostrar,
            'confirmar' => $confirmar,
            'mensaje' => $mensaje,
            'disabled' => $disabled,
            'clipboard' => $clipboard
        ];
    }

    /**
     * Establece un ítem con un BOTON único
     *
     * @param $idDato
     * @param $boton
     * @param $clase
     * @param $atributo
     * @param $valor_atributo
     */
    public function addDataButtonItem($idDato, $boton, $clase = null, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'button', $boton, $clase, $atributo, $valor_atributo);
    }

    /**
     * Establece un item de tipo BOTONES a un dato en específico
     *
     * @param  string $idDato
     * @param  array $arrayBotones
     * @param  string $clase
     *
     * @return void
     */
    public function addDataButtonsItem($idDato, array $arrayBotones, $clase = null){
        $this->addDataItem($idDato, 'botones', $arrayBotones, $clase);
    }

    /**
     * Establece un item de tipo PETICIÓN a un dato en específico
     *
     * @param  string $idDato
     * @param  string $accion
     *
     * @return void
     */
    public function addDataRequestItem($idDato, $accion){
        $this->addDataItem($idDato, 'peticion', [
            'accion' => $accion
        ]);
    }

    /**
     * Establece un item de tipo FORM-TABLE para un dato en específico
     *
     * @param $idDato
     * @param $form
     * @param $table
     */
    public function addDataFormTableItem($idDato, $form, $table){
        $this->addDataItem($idDato, 'formtabla', [
            'form' => $form,
            'table' => $table
        ]);
    }

    /**
     * Crea un elemento para el arreglo Dataset de las gráficas
     *
     * @param  string $texto
     * @param  mixed $dato -> array: line-chart, bar-chart | number: polar-area-chart, pie-chart, doughnut-chart
     * @param  string $clase
     *
     * @return array
     */
    public function createDatasetItem($texto, $dato, $clase){
        return [
            'label' => $texto,
            'data' => $dato,
            'clase' => $clase
        ];
    }

    /**
     * Establece un item de tipo GRÁFICA LINEAL a un dato en específico
     *
     * @param  string $idDato
     * @param   array $arrayLabels
     * @param   array $arrayDataset
     *
     * @return void
     */
    public function addDataLineChartItem($idDato, array $arrayLabels, array $arrayDataset, $atributo = null, $valor_atributo = null){
        $this->addDataItem($idDato, 'grafica', [
            'tipo_grafica' => 'line-chart',
            'labels' => $arrayLabels,
            'dataset' => $arrayDataset
        ], null, $atributo, $valor_atributo);
    }

    /**
     * Establece un item de tipo GRÁFICA DE BARRAS a un dato en específico
     *
     * @param  string $idDato
     * @param   array $arrayLabels
     * @param   array $arrayDataset
     *
     * @return void
     */
    public function addDataBarChartItem($idDato, array $arrayLabels, array $arrayDataset){
        $this->addDataItem($idDato, 'grafica', [
            'tipo_grafica' => 'bar-chart',
            'labels' => $arrayLabels,
            'dataset' => $arrayDataset
        ]);
    }

    /**
     * Establece un item de tipo GRÁFICA DE ÁREA POLAR a un dato en específico
     *
     * @param  string $idDato
     * @param   array $arrayDataset
     *
     * @return void
     */
    public function addDataPolarAreaChartItem($idDato,  array $arrayDataset){
        $this->addDataItem($idDato, 'grafica', [
            'tipo_grafica' => 'polar-area-chart',
            'dataset' => $arrayDataset
        ]);
    }

    /**
     * Establece un item de tipo GRÁFICA DE PASTEL a un dato en específico
     *
     * @param  string $idDato
     * @param   array $arrayDataset
     *
     * @return void
     */
    public function addDataPieChartItem($idDato,  array $arrayDataset){
        $this->addDataItem($idDato, 'grafica', [
            'tipo_grafica' => 'pie-chart',
            'dataset' => $arrayDataset
        ]);
    }

    /**
     * Establece un item de tipo GRÁFICA DE DONA a un dato en específico
     *
     * @param  string $idDato
     * @param   array $arrayDataset
     *
     * @return void
     */
    public function addDataDoughnutChartItem($idDato,  array $arrayDataset){
        $this->addDataItem($idDato, 'grafica', [
            'tipo_grafica' => 'doughnut-chart',
            'dataset' => $arrayDataset
        ]);
    }

    /**
     * Establece un item de tipo JSON en el valor
     *
     * @param $idDato
     * @param $json
     */
    public function addDataJsonItem($idDato, $json){
        $this->addDataItem($idDato, 'json', $json);
    }

    /**
     * Devuelve los items de un dato en específico
     *
     * @param  string $idDato
     * @return array items
     */
    public function getDataItems($idDato){
        return $this->datos[$idDato]['items']?:null;
    }

    /**
     * Devuelve el objeto con un formato JSON
     *
     * @return mixed json
     */
    public function toJson(){
        $pre_format = $this->pre_format();

        $json = [];
        foreach ($pre_format['datos'] as $id => $dato){
            $item['ID'] = $id;
            $index = 0;
            foreach ($pre_format['headers'] as $nombre => $header){
                $nombre = $this->normalize($nombre);

                switch ($dato['items'][$index]['tipo']) {
                    case 'texto':
                    case 'json':
                        $item[$nombre] = isset($dato['items'][$index]['valor'])
                            ? $dato['items'][$index]['valor']
                            : null;
                        break;
                    case 'label':
                        $item[$nombre] = isset($dato['items'][$index]['valor']['mensaje'])
                            ? $dato['items'][$index]['valor']['mensaje']
                            : null;
                        break;
                    case 'button':
                        $item[$nombre] = isset($dato['items'][$index]['valor']['valor'])
                            ? $dato['items'][$index]['valor']['valor']
                            : null;
                        break;
                }
                $index++;
            }
            $json[] = $item;
        }

        return Response::json($json);
    }

    /**
     * Devuelve el objeto con un formato JSON, utilizado para construir las tablas
     *
     * @return string jsonTabla
     */
    public function toJsonTabla(){
        $pre_format = $this->pre_format();

        $json = [];
        foreach ($pre_format['datos'] as $id => $dato){
            $dato['id'] = $id;
            $json[] = $dato;
        }

        $arrayTabla = [
            'parametros' => $this->parametros,
            'menu' => $this->menu,
            'order_by' => $this->order_by,
            'headers' => $pre_format['headers'],
            'datos' => $json
        ];

        return Response::json($arrayTabla);
    }

    /**
     * Devuelve el objeto con un formato CSV
     * @param bool $archivo
     * @return string
     */
    public function toCSV($archivo = false){
        $pre_format = $this->pre_format();
        $csv = '';

        foreach (array_keys($pre_format['headers']) as $header){
            $csv .= '"'. str_replace('"', '\"', $this->normalize($header)) .'",';
        }
        $csv .= (chr(0x0D).chr(0x0A));

        foreach($pre_format['datos'] as $id => $dato){
            foreach($dato['items'] as $item){
                switch ($item['tipo']){
                    case 'texto':
                    case 'html':
                        $csv .= '"'. str_replace('"', '\"', $item['valor']) .'",';
                        break;
                    case 'label':
                        $csv .= '"'. str_replace('"', '\"', $item['valor']['mensaje']) .'",';
                        break;
                    case 'botones':
                        $botones = '';
                        foreach ( $item['valor'] as $boton ){
                            $botones .= $boton['titulo'].': '.$boton['accion'].', ';
                        }
                        $csv .= '"'. str_replace('"', '\"', $botones) .'",';
                        break;
                    case 'grafica':
                        $csv .= 'Elemento gráfico';
                        break;
                    case 'link':
                        $csv .= '"'. str_replace('"', '\"', $item['valor']['mensaje']) .': '. $item['valor']['accion'] .'",';
                        break;
                    case 'peticion':
                        $csv .= '"'. str_replace('"', '\"', $item['valor']['accion']) .'",';
                        break;
                }
            }
            $csv .= (chr(0x0D).chr(0x0A));
        }

        if($archivo){
            // DESCARGAR ARCHIVO
            $nombre =  $this->parametros['titulo'].".csv";
            $response = \Response::make($csv, 200);
            $response->header("Content-Type", "application/vnd.ms-excel");
            $response->header("Content-Disposition", "attachment; filename=$nombre");
            $response->header('Pragma', 'public');

            ob_end_clean();

            return $response;
        }
        else{
            return $csv;
        }
    }

    /**
     * Devuelve el resultado en el formato solicitado
     *
     * @param null $extension
     * @return null|string
     */
    public function respuesta($extension = null, $archivo = false){
        $respuesta = null;
        switch ($extension){
            case '.jsonTabla':
                $respuesta = $this->toJsonTabla();
                break;
            case '.csv':
                $respuesta = $this->toCSV($archivo);
                break;
            case '.json':
            default:
                $respuesta = $this->toJson();
                break;
        }

        return $respuesta;
    }

    /**
     * Obtiene los headers necesarios, en caso de necesitarlos
     *
     * @return array
     */
    private function pre_format(){

        $nuevosDatos = $this->datos;
        $nuevosHeaders = $this->headers;

        // Obtiene los indices que tienen al menos un dato
        $headersItems = [];
        foreach ( $nuevosDatos as $indiceDato => $dato ){
            foreach ($dato['items'] as $indiceItem => $item){
                if($item['valor'] !== null){
                    $headersItems[$indiceItem][] = $indiceDato;
                }
            }
        }

        // Elimina de los headers los que no son necesarios
        $indice = 0;
        foreach($nuevosHeaders as $header => $valor){
            if( ! array_key_exists($indice, $headersItems) ){
                unset($nuevosHeaders[$header]);
            }
            $indice++;
        }

        // Elimina de datos los que no son necesarios
        foreach ($nuevosDatos as $indiceDato => $dato){
            foreach (array_keys(array_diff_key($dato['items'], $headersItems)) as $key){
                unset($nuevosDatos[$indiceDato]['items'][$key]);
            }
            $nuevosDatos[$indiceDato]['items'] = array_values($nuevosDatos[$indiceDato]['items']);
        }

        return [
            'headers' => $nuevosHeaders,
            'datos' => $nuevosDatos
        ];

    }

    private function normalize($str){
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '%' => 'porcentaje' );

        $str =  strtr( $str, $unwanted_array );
        $str = str_replace(" ", "_", $str);
        $str = strtoupper($str);

        return $str;
    }

}