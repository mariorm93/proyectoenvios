<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
require(app_path().'/estados.php');

class Formulario_json extends Controller
{
    public function formularioToJson(){

    	$totalEstados='';
    	$i=1;

    	foreach (\Estados::$id as $idEstado => $sifnificado) {
    		if($i==1){
    			$estados="\n".'{'."\n"."\"check\"".":"."false,\n"."\"texto\"".":".'"'.$idEstado.'=>'.$sifnificado.'"'.",\n"."\"valor\":"."\"$idEstado\""."\n".'}';
    			++$i;
    		}else{
    			$estados=","."\n".'{'."\n"."\"check\"".":"."false,\n"."\"texto\"".":".'"'.$idEstado.'=>'.$sifnificado.'"'.",\n"."\"valor\":"."\"$idEstado\""."\n".'}';
    		}
    		$totalEstados=$totalEstados.$estados;
    	}

    	//dd($totalEstados);
    	
    	$cadenaJson='{
	"tipo": "formulario",
	"formulario": "Envios_a_Comparar",
	"titulo": "Escribe los Números de Envíos a Comparar",
	"botones": [{
			"accion": "",
			"destino": "limpiar",
			"texto": "Limpiar",
			"clase": "btn-default"
		},

		{
			"accion": "",
			"destino": "emit",
			"texto": "Comparar",
			"clase": "btn-primary"
		}



	],
	"secciones": [{
		"inputs": [{
				"tipo": "number",
				"nombre": "id_Envio_1",
				"id": "id_Envio_1",
				"texto": "id del Envío 1"
				}
				,
				{
				"tipo": "number",
				"nombre": "id_Envio_2",
				"id": "id_Envio_2",
				"texto": "id del Envío 2"
				}
			]
		}	
		,
		{
		"inputs": [{
				"tipo": "radio",
				"id": "estados_a_elegir",
				"nombre": "seleccion_de_estados[]",
				"texto": "Solo muestra campos con valor",
				"checked": true
				}
				,
				{
				"tipo": "radiodinamico",
				"id": "estados_a_elegir",
				"nombre": "seleccion_de_estados[]",
				"texto": "Seleccionar estados a mostrar",
				"checked": false,
				"inputs": [{
					"id": "todos_los_envios",
					"nombre": "seleccion[]",
					"texto": "Selecciona los Estados que quieres comparar de tus Envíos:",
					"tipo": "multiselect",
					"valor": ['.$totalEstados.']
					}]
				}
			]
		}
	]
}';
    	
		//dd($cadenaJson);

    	$jason=json_decode($cadenaJson);
    	return response()->json($jason);
    }






}
