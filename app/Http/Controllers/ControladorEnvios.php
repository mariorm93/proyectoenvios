<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Users;
require(app_path().'/estados.php');
use App\Resultado;


//\;

class ControladorEnvios extends Controller
{
    
	
	public function doubleToTime($number)
	{
	    $dias=(integer)$number;
	    $dias-=25569;
	    $secs=$dias*24*3600;
	    $tiempo=$number-(integer)$number;
	    $tiempo=$tiempo*24*3600;
	    $secs+=$tiempo;
	    return (integer)$secs;
	}

	public function queryTablMensEnvios($idEnvio){


		$tablaEnvios=DB::table('envios')
					->select('fechaInicio', 'id', 'fechaUltimoMovimiento')
					->where('id', '=', $idEnvio)
					->get();


		//realizamos las funciones para saber el espacio de tiempo a evaluar del Envio1
		$fechaInicio=$tablaEnvios->first()->fechaInicio;
		$fechaFinal=$tablaEnvios->first()->fechaUltimoMovimiento;

		
	    $fechaInicio = $this->doubleToTime($fechaInicio);
		$fechaFinal = $this->doubleToTime($fechaFinal);


	    $fechaInicio=new \DateTime("@$fechaInicio");
    	$fechaFinal=new \DateTime("@$fechaFinal");
	    

    
	    //declarar la colección donde voy a almacenar la unión de los resultados
		$coleccionQueries=collect();

		//Realizo las Consultas

	    while($fechaInicio<=$fechaFinal){

	    	$fechaIndex = $fechaInicio->format("ym");
    		
    		//declarar el resultado de ejecutar el query (esto es una colección)
	    	$arregloQueries=DB::table("mensajesenvios_$fechaIndex")
	                    ->select('estado','destino')
	                    ->where("idEnvio","=", "$idEnvio")
	                    //->limit(10)
	                    ->get();

            //agregr los elementos del resultado a una colección general
			

	        $coleccionQueries=$coleccionQueries->union($arregloQueries);

	        date_add($fechaInicio, date_interval_create_from_date_string('1 months'));

	    }
 		

	    return($coleccionQueries);

	}

	public function cambiadorLlaves( $coleccion, $nueva_key ) {



		$nuevoarray=array();
	  

		foreach ($coleccion as $key => $registro) {
			//asigno el valor de la nueva llave
			$nueva_llave=$registro->$nueva_key;
			//dd($nueva_llave);

			if(!array_key_exists($nueva_llave, $nuevoarray)) {
				$nuevoarray[$registro->destino] = $registro->estado;
			}
		}
		return($nuevoarray);  
	}






	//Request $request
	public function estadosUsar(Request $request){

		//Pongo en un array los estados que quiere el usuario mostrar
		
		
		
		$estadosrequest=$request->input('seleccion');
		$estadosElegidos=array();

		
		

		//dd($estadosElegidos);
		if($estadosrequest!=null){

			foreach ($estadosElegidos as $key => $value) {

				if(!array_key_exists($key, \Estados::$id)){
					unset($estadosElegidos[$key]);
				}
			}

			foreach ($estadosrequest as $llave => $estadoElegido) {
				$estadosElegidos[$estadoElegido]="";
			}
			//dd($estadosElegidos);
			return($estadosElegidos);
		}

		$estadosElegidos=array();

		//dd($estadosElegidos);

		return($estadosElegidos);
	}







	//							//($idEnvio1,$idEnvio2){
	public function comparador($idEnvio1,$idEnvio2, $estadosElegidos){
		//$idEnvio1=34716866;	
		//$idEnvio2=34716973;

		//obtengo las colecciones a comparar

		$coleccionEnvio1=$this->queryTablMensEnvios($idEnvio1);
		
		$coleccionEnvio2=$this->queryTablMensEnvios($idEnvio2);


		// cambio las Keys de las colecciones a los destinos
		//Nota:esta funcion regresa arreglos
		$arrayEnvio1=$this->cambiadorLlaves($coleccionEnvio1,'destino');

		

		$datosFinales=array();

		//$estadosElegidos=$this->estadosUsar();



		//dd($arrayEnvio1);

		
		if(empty($estadosElegidos)==false){
			//realizo el array de dimensión 2 para guardar los datos

			foreach (\Estados::$id as $key => $estadosComparar){

				if(array_key_exists($key, $estadosElegidos)){
					$datosFinales[$key]=array();
				
					foreach (\Estados::$id as  $key2 => $valor2) {
						if(array_key_exists($key2, $estadosElegidos)){
							$datosFinales[$key][$key2]=0;
						}
					}
				}
			}

		}else{

			foreach ($coleccionEnvio2 as $valor) {

				$destino2=$valor->destino;
				$key=$valor->estado;

				if(!array_key_exists($key, $datosFinales)){
					$datosFinales[$key]=array();
					
				}

				

				if (!array_key_exists($destino2, $arrayEnvio1)) {

					
					$key2=$arrayEnvio1[$destino2];

					
					
					if(!array_key_exists($key2, $datosFinales[$key])){
						$datosFinales[$key][$key2]=0;
					}

					//$datosFinales[$key]=array($key2);

					$datosFinales[$destino2]["NA"]+=1;

				} else {

					$estado1=$arrayEnvio1[$destino2];

					if($estado1!='usado'){

						$key2=$estado1;
						

						//$datosFinales[$key2]=array($key=>0);

						if( !array_key_exists($key2, $datosFinales) or !array_key_exists($key, $datosFinales[$key2]) ) {									
							$datosFinales[$key2][$key]=0;
							
						}
						
						
						$datosFinales[$valor->estado][$key2]+=1;
						$arrayEnvio1[$destino2]='usado';

					}
				}	
			}


			foreach ($arrayEnvio1 as $value) {

				if($value!='usado'){

					if(array_key_exists($value, $estadosElegidos)){

						$datosFinales["NA"][$value]+=1;
					}
				}
			}


			foreach ($datosFinales as $i => $value) {

				foreach ($datosFinales as $j => $value) {

					if(!array_key_exists($j, $datosFinales[$i])){
						
						$datosFinales[$i][$j]=0;

					}
				}
				
			}

			foreach ($datosFinales as $key => $value) {
				ksort($datosFinales[$key]);
			}

			

			
			
			
			return($datosFinales);


			

				
			
		}

		//filtrado de datos por destino
		foreach ($coleccionEnvio2 as $valor) {



			if(array_key_exists($valor->estado, $estadosElegidos)){

				$destino2=$valor->destino;

				if (!array_key_exists($destino2, $arrayEnvio1)) {
					$datosFinales[$destino2]["NA"]+=1;
				} else {
					$estado1=$arrayEnvio1[$destino2];

					if($estado1!='usado'){
						$datosFinales[$valor->estado][$estado1]+=1;
						$arrayEnvio1[$destino2]='usado';
					}
				}
			}
		}





		foreach ($arrayEnvio1 as $value) {

			if($value!='usado'){

				if(array_key_exists($value, $estadosElegidos)){

					$datosFinales["NA"][$value]+=1;
				}
			}
		}



		
		
		return($datosFinales);

		
	}
	



	
	public function pintado(Request $request){
		//$idEnvio=34716866;	
		//$idEnvio2=34716973;

		ini_set('memory_limit','2048M'); // Aumento la memoria a como un giga :desconcertado:
       	ini_set('max_execution_time', 300); // Aumento el tiempo de ejecucion (5 mins)
		
		
		$idEnvio = $request->input('id_Envio_1');
		$idEnvio2 = $request->input('id_Envio_2');
		
		
		$resultado = new Resultado([

	       	'titulo' => "Tabla Comparativa",

	        	'clase' => 'table table-stripped'

	    	]);


		

		if($idEnvio==null or $idEnvio==null){


			
			$resultado->setTableParam('vacio', 'No has enviado algún Id de Envío');
			return $resultado->toJsonTabla();

		}

		




		$estadosElegidos=$this->estadosUsar($request);

		$tablaComparada=$this->comparador($idEnvio, $idEnvio2, $estadosElegidos);

		



		if(empty($estadosElegidos)==true){
			foreach ($tablaComparada as $key => $valor) {
				
				$estadosElegidos[$key]="";
				
			}
		}


		

		//dd($estadosElegidos);


		// Crear HEADERS de la Tabla

	    
		$resultado->addHeader('Envio2/Envio1');



		foreach(\Estados::$id as $llave => $valor){
			
			if(array_key_exists($llave, $estadosElegidos)){
				$resultado->addHeader("$valor ($llave)");
			}
		}


	    
	    // Agregar datos por FILA
	    
	    foreach ($tablaComparada as $estado2 => $row) {

	    	$resultado->addDataTextItem($estado2, \Estados::$id[$estado2]);
	    	foreach ($row as $estado1 => $contador) {
	    		$resultado->addDataTextItem($estado2, $contador);
	    	}	
	    }

	    
	    // Devuelve los datos como un JSON que puede interpretar VUE

	    return $resultado->toJsonTabla();
	}


}
