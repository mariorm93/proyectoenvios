<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('comparador', 'ControladorEnvios@comparador');
Route::get('pruebas', 'ControladorEnvios@pintado');
Route::get('formulario', 'Formulario_json@formularioToJson');


Route::get('/tabla', function () {
    return view('tabla');
});
